﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Common.Dtos;
using System.Text;

namespace RabbitMQ.Consumer
{
    public static class DirectExchangeConsumer
    {
        public static void Consume(IModel channel)
        {
            channel.ExchangeDeclare("demo-direct-exchange", ExchangeType.Direct);
            channel.QueueDeclare("demo-direct-queue", durable: true, exclusive: false, autoDelete: false, arguments: null);
            channel.QueueBind("demo-direct-queue", "demo-direct-exchange", "demo.routing");
            channel.BasicQos(0, 10, false);

            var consumner = new EventingBasicConsumer(channel);
            consumner.Received += (sender, arg) =>
            {
                var body = arg.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine($" - Received[{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}]: {JsonConvert.SerializeObject(message)}");
                var data = JsonConvert.DeserializeObject<MessageDto>(message);

                // thử throw exception để consumer ko handle được message này
                if (data.Id == 2) throw new Exception("No handle this message");

                // ví dụ consumer xử lý dừng 1 phút
                Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);

                Console.WriteLine($" => Proccessed[{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}]: {JsonConvert.SerializeObject(message)}");
            };
            channel.BasicConsume("demo-direct-queue", true, consumner);
            Console.ReadLine();

        }
    }
}
