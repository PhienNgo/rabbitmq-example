﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Consumer;
using System.Text;

Console.WriteLine("Consumer start!");
var factory = new ConnectionFactory
{
    Uri = new Uri("amqp://guest:guest@localhost:5672")
};

using var connection = factory.CreateConnection();
using var channel = connection.CreateModel();
DirectExchangeConsumer.Consume(channel);

//channel.QueueDeclare("demo-queue", durable: true, exclusive: false, autoDelete: false, arguments: null);
//QueueConsumer.Consume(channel);
