﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Common.Dtos;
using System.Text;

namespace RabbitMQ.Producer
{
    public static class DirectExchangePublisher
    {
        public static async void Publish(IModel channel)
        {
            Dictionary<string, object> ttl = new Dictionary<string, object>()
            {
                { "x-message-ttl",10000}
            };

            channel.ExchangeDeclare("demo-direct-exchange", ExchangeType.Direct, arguments: ttl);

            for (int i = 0; i < 5; i++)
            {
                PublishMessage(channel, i);
            }
            Console.WriteLine("Publish completed!");
        }
        private static void PublishMessage(IModel channel, int id)
        {
            var message = new MessageDto { Id = id, GUID = Guid.NewGuid(), Time = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") };
            Console.WriteLine($"Send: {JsonConvert.SerializeObject(message)}");
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));
            channel.BasicPublish("demo-direct-exchange", "demo.routing", null, body);
        }
    }
}
