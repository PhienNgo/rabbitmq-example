﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Producer;
using System.Text;

Console.WriteLine("Producer start!");
var factory = new ConnectionFactory
{
    Uri = new Uri("amqp://guest:guest@localhost:5672")
};

using var connection = factory.CreateConnection();
using var channel = connection.CreateModel();
DirectExchangePublisher.Publish(channel);


//channel.QueueDeclare("demo-queue", durable: true, exclusive: false, autoDelete: false, arguments: null);
//int count = 0;
//while (true)
//{
//    QueueProducer.Publish(channel);
//    count++;
//    Thread.Sleep(1000);
//}
