﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Common.Dtos
{
    public class MessageDto
    {
        public int Id { get; set; }
        public Guid GUID { get; set; }
        public string Time { get; set; }
    }
}
