# RabbitMQ Demo App


## Producer

Publish message to RabbitMQ Server

## Consumer

Receive message from RabbitMQ Server and log it to console


## Guideline to Run
- From \RabbitMQ.Producer run 2 commands:
```
dotnet build
dotnet run
```
=> we will see log 5 message published

- From \RabbitMQ.Consumer run 2 commands: 
```
dotnet build
dotnet run
```
=> we will see log of 5 message that was published by producer. Each message take 10 seconds to Process


That is my prastice, please take a look on that hi.

Thanks,
